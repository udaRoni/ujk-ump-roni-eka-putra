package com.example.assementjmp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    TextView txtTanggal, txtJam, txtHarga, txtMenu;
    EditText editTNM;
    Button btnMenu, btnProceed;
    Spinner spinnerKM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtTanggal = (TextView) findViewById(R.id.txtTanggal);
        txtJam = (TextView) findViewById(R.id.txtJam);
        txtHarga = (TextView) findViewById(R.id.txtHarga);
        txtMenu = (TextView) findViewById(R.id.txtMenu);
        editTNM = (EditText) findViewById(R.id.editTNM);
        btnMenu = (Button) findViewById(R.id.btnMenu);
        btnProceed = (Button) findViewById(R.id.btnProceed);

        spinnerKM = (Spinner) findViewById(R.id.spinnerKM);
        spinnerKM.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String txtSpinner = spinnerKM.getSelectedItem().toString();

                if(txtSpinner.equals("")){
                    txtHarga.setText(null);
                    txtMenu.setText(null);
                }else if(txtSpinner.equals("B01")){
                    txtHarga.setText("RP. 10.000");
                    txtMenu.setText("Kopi Hitam");
                }else if(txtSpinner.equals("B02")){
                    txtHarga.setText("RP. 20.000");
                    txtMenu.setText("Cappucino");
                }else if(txtSpinner.equals("B03")){
                    txtHarga.setText("RP. 15.000");
                    txtMenu.setText("Sparkling Tea");
                }else if(txtSpinner.equals("F01")){
                    txtHarga.setText("RP. 25.000");
                    txtMenu.setText("Batagor");
                }else if(txtSpinner.equals("F02")){
                    txtHarga.setText("RP. 10.000");
                    txtMenu.setText("Cireng");
                }else if(txtSpinner.equals("F03")){
                    txtHarga.setText("RP. 50.000");
                    txtMenu.setText("Nasi Goreng");
                }else if(txtSpinner.equals("D01")){
                    txtHarga.setText("RP. 40.000");
                    txtMenu.setText("Cheese Cake");
                }else{
                    txtHarga.setText("RP. 30.000");
                    txtMenu.setText("Black Salad");
                }
                updateTime();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btnMenu.setOnClickListener(this);
        btnProceed.setOnClickListener(this);
    }
    private void addOrder(){
        final String nomorMeja = editTNM.getText().toString().trim();
        final String kodeMenu  = spinnerKM.getSelectedItem().toString().trim();
        final String menu = txtMenu.getText().toString().trim();
        final String harga = txtHarga.getText().toString().trim();


        class AddOrder extends AsyncTask<Void,Void,String> {

            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(MainActivity.this,"Menambahkan...","Tunggu...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(MainActivity.this,s,Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(Void... v) {
                HashMap<String,String> params = new HashMap<>();
                params.put(konfigurasi.KEY_EMP_NOMOR,nomorMeja);
                params.put(konfigurasi.KEY_EMP_KODE,kodeMenu);
                params.put(konfigurasi.KEY_EMP_MENU,menu);
                params.put(konfigurasi.KEY_EMP_HARGA,harga);

                RequestHandler rh = new RequestHandler();
                String res = rh.sendPostRequest(konfigurasi.URL_ADD, params);
                return res;
            }
        }

        AddOrder ae = new AddOrder();
        ae.execute();
    }


    @Override
    public void onClick(View v) {
        if(v == btnMenu){
            Intent intent = new Intent(MainActivity.this,MenuCafe.class);
            startActivity(intent);
        }
        if(v == btnProceed){
            addOrder();
            Bundle bundle = new Bundle();
            bundle.putString("data1", txtMenu.getText().toString());
            bundle.putString("data2", txtHarga.getText().toString());
            Intent intent = new Intent(MainActivity.this,ListCurrentOrder.class);
            intent.putExtras(bundle);
            startActivity(intent);

        }
    }

    public void updateTime(){
        SimpleDateFormat date = new SimpleDateFormat("yyyy.MM.dd");
        String currentDate = date.format(new Date());
        txtTanggal.setText(currentDate);
        SimpleDateFormat time = new SimpleDateFormat("HH:mm:ss ");
        txtJam.setText(time.format(new Date()));
    }

}
