package com.example.assementjmp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class ListCurrentOrder extends AppCompatActivity {

    TextView txtOrder;
    TextView txtPriceTotal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_current_order);
        txtOrder = (TextView) findViewById(R.id.txtOrder);
        txtPriceTotal = (TextView) findViewById(R.id.txtPriceTotal);

        if(getIntent().getExtras()!=null){
            /**
             * Jika Bundle ada, ambil data dari Bundle
             */
            Bundle bundle = getIntent().getExtras();
            txtOrder.setText(bundle.getString("data1"));
            txtPriceTotal.setText(bundle.getString("data2"));
        }else{
            /**
             * Apabila Bundle tidak ada, ambil dari Intent
             */
            txtOrder.setText("Anda Memesan : "+getIntent().getStringExtra("data1"));
            txtPriceTotal.setText("Total Harga : RP. "+getIntent().getStringExtra("data2"));
        }
    }
}
