package com.example.assementjmp;

public class konfigurasi {
    public static String BASE_URL = "http://192.168.214.113";
    public static final String URL_ADD= BASE_URL + "/assessment/tambahPesanan.php";
    public static final String URL_GET_ALL = BASE_URL + "/assessment/tampilSemuaPesanan.php";


    public static final String KEY_EMP_NOMOR= "nomorMeja";
    public static final String KEY_EMP_KODE= "kodeMenu";
    public static final String KEY_EMP_MENU = "menu";
    public static final String KEY_EMP_HARGA = "harga";

    //JSON Tags
    public static final String TAG_JSON_ARRAY="result";
    public static final String TAG_NOMOR="nomorMeja";
    public static final String TAG_KODE= "kodeMenu";
    public static final String TAG_MENU = "menu";
    public static final String TAG_HARGA = "harga";
}
